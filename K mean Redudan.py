#digunakan untuk matriks
import numpy as np
#digunakan untuk membuat grafik
import matplotlib.pyplot as plt
#import cvs
import csv
#normal
from sklearn.preprocessing import normalize
import pandas as pd

#fungsi mengeload data set
def load_data(nama):
    #load data Set
    df = pd.read_csv(nama)
    #mengganti tanda tanya ke NaN numpy
    new_df = df.replace('?',np.NaN)
    new = pd.DataFrame(new_df)
    #menghapus data yang berhisi NaN
    baru = new.dropna()
    value = baru.as_matrix()
    #menormalalisasi
    x2= normalize(value[:,[1,2,3,4,5,6,7,8,9]],axis=0)
    x3 = np.asarray(np.delete(value, [0,1,2,3,4,5,6,7,8,9], 1), int)
    contate=np.append(x2,x3,axis=1)
    #menghapus data yang sama
    sudah=np.unique(contate,axis=0)
    data = np.asarray(np.delete(sudah,[9],1),float)
    return data

#mengambil class
def classnya(nama):
    df = pd.read_csv(nama)
    new_df = df.replace('?',np.NaN)
    new = pd.DataFrame(new_df)
    baru = new.dropna()
    value = baru.as_matrix()
    print
    x2= normalize(value[:,[1,2,3,4,5,6,7,8,9]],axis=0)
    x3 = np.asarray(np.delete(value, [0,1,2,3,4,5,6,7,8,9], 1), int)
    contate=np.append(x2,x3,axis=1)
    sudah=np.unique(contate,axis=0)
    data = np.asarray(np.delete(sudah,[0,1,2,3,4,5,6,7,8],1),float)
    return data


#fungsi Untuk menghitung jarak
def jarak(a,b):
    return np.linalg.norm(a-b)

#fungsi untuk menggambar grafik
def plot(dataset, history_centroids, belongs_to):
    #memlih warna
    colors = ['r','g','y','k']

    fig, ax = plt.subplots()

    #digambar berdasarkan cluster
    for index in range(dataset.shape[0]):
        instances_close = [i for i in range(len(belongs_to)) if belongs_to[i] == index]
        for instance_index in instances_close:
            ax.plot(dataset[instance_index][0], dataset[instance_index][1], (colors[index] + 'o'))

    #menggambar titik centroid
    history_points = []
    for index, centroids in enumerate(history_centroids):
        for inner, item in enumerate(centroids):
            if index == 0:
                history_points.append(ax.plot(item[0], item[1], 'bo')[0])
            else:
                history_points[inner].set_data(item[0], item[1])
                print("centroids {} {}".format(index, item))

                plt.pause(0.8)

def kmean():
    #epsilon adalah gagal
    epsion=0
    #membuat catatan centroid
    catatan_centroid = []
    #meload data
    databaru=load_data('cancer.txt')
    print databaru
    # print databaru
    # memisahkan jumalh record dan field
    jumlah_record,jumlah_field = databaru.shape
    #inisisasi centroid
    centroid_terpilih = []
    #memilih centroid
    jumlah_centroid = input("Masukan Jumlah Centroid\n")
    pilihan = input("1.Random\n2.Pilih Sendiri\n")
    if pilihan==1:
        proto = databaru[np.random.randint(0, jumlah_record - 1, size=jumlah_centroid)]
    else:
        for x in range(jumlah_centroid):
            pilih = input("Centroid yang "+repr(x+1)+"\n")
            centroid_terpilih.append(pilih)
            # print centroid_terpilih
        #mengambil centroid terpilih
        proto = databaru[[centroid_terpilih]]

    print proto
    #memasukan dalam catatan centroid
    catatan_centroid.append(proto)
    #membuat dummy pengurangan
    proto_lama = np.zeros(proto.shape)
    #membuat cluster dammy
    cluster = np.zeros((jumlah_record, 1))
    #menghitung jarak pertama
    jaraknya = jarak(proto,proto_lama)
    iterasi =0
    while jaraknya>epsion:
        iterasi+=1
        #menghitung jarak
        jaraknya = jarak(proto,proto_lama)
        proto_lama =proto
        for index_content, content in enumerate(databaru):
            #inisiasi matrik jarak
            jaraksetiap=np.zeros((jumlah_centroid,1))
            for index_proto, content_proto in enumerate(proto):
                #mengisi jarak
                jaraksetiap[index_proto] = jarak(content_proto,content)

            #memilih jarak yang paling kecil
            cluster[index_content]=np.argmin(jaraksetiap)

        #ini siasi tempat penyimpanan
        tmp_proto = np.zeros((jumlah_centroid,jumlah_field))

        for index in range(len(proto)):
            #memilihkan cluster
            instances_close = [i for i in range(len(cluster)) if cluster[i] == index]
            #mencari rata2
            proto = np.mean(databaru[instances_close], axis=0)
            # prototype = dataset[np.random.randint(0, num_instances, size=1)[0]]
            tmp_proto[index, :] = proto
        #mengeset jarak yang baru
        # print proto
        proto = tmp_proto
        #menyimpan di cetatan
        catatan_centroid.append(tmp_proto)

    return databaru,catatan_centroid,cluster,jumlah_centroid,jumlah_record



datame, catatan_centroid,cluster ,jumlah_centroid,jumlah_record=kmean()
plot(datame, catatan_centroid, cluster)
clasnya = classnya('cancer.txt')
hitung=np.append(cluster,clasnya,axis=1)
# print hitung[1]
# hh=(hitung[1]==[float(0.0) , float(4.0)])
# print hh

for y in range(jumlah_centroid):
    dua = 0
    empat = 0
    for x in range(jumlah_record):
          if (hitung[x]==[float(y) ,float(2)]).all():
              dua+=1
          elif (hitung[x]==[float(y) ,float(4)]).all():
              empat+=1
    print "untuk Cluseter" + repr(y+1)+"\n"
    hasil_dua = (float(dua)/jumlah_record)
    print "2 =" + repr(dua)
    print "Persentasenya " + repr(dua)+"/"+repr(jumlah_record)+"="+repr(hasil_dua*100)+"%\n"
    hasil_empat = (float(empat) / jumlah_record)
    print "4 =" + repr(empat)
    print "Persentasenya " + repr(empat) + "/" + repr(jumlah_record) + "=" + repr(hasil_empat*100)+"%\n"
